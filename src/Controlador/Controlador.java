package Controlador;
import Modelo.Recibo;
import Vista.dlgVista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
public class Controlador implements ActionListener{
    private Recibo reci;
    private dlgVista vista;

    public Controlador (Recibo reci, dlgVista vista) {
        this.reci = reci;
        this.vista = vista;
        
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.cmbTipoServicio.addActionListener(this);
    }
    
    public void iniciarVista(){
        vista.setTitle(":: RECIBO ::");
        vista.setSize(600, 500);
        vista.setVisible(true);
    }
    
    public void limpiar(){
        vista.txtCostoK.setText("");
        vista.txtDomicilio.setText("");
        vista.txtFecha.setText("");
        vista.txtImpuesto.setText("");
        //vista.cbPlazo.setSelectedItem(" ");
        vista.cmbTipoServicio.setSelectedIndex(0);
        vista.txtKilowats.setText("");
        vista.txtNombre.setText("");
        vista.txtNumRecibo.setText("");
        vista.txtSubTotal.setText("");
        vista.txtTotal.setText("");
    }
    public boolean isVacio(){
        if(vista.txtNombre.getText().isEmpty() ||vista.txtDomicilio.getText().isEmpty()|| vista.txtFecha.getText().isEmpty()|| vista.txtCostoK.getText().isEmpty()|| vista.txtKilowats.getText().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if(e.getSource()==vista.btnNuevo){
            vista.btnCancelar.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.cmbTipoServicio.setEnabled(true);
            vista.txtCostoK.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtKilowats.setEnabled(true);
            vista.txtNumRecibo.setText(String.valueOf(reci.getNumRecibo()));
            limpiar();
        }
        else if(e.getSource()==vista.btnCancelar){
            limpiar();
            vista.btnCancelar.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.cmbTipoServicio.setEnabled(true);
            vista.txtCostoK.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtKilowats.setEnabled(true);
            vista.txtNumRecibo.setText(String.valueOf(reci.getNumRecibo()));
        }
        else if(e.getSource()==vista.btnLimpiar){
            limpiar();
        }
        else if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista, "¿Quiere salir del programa?",
                    "Decide",JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
        
        else if(e.getSource()==vista.btnGuardar){
            if(isVacio()==true){
                JOptionPane.showMessageDialog(vista, " CAMPOS VACIOS ");
            }
            else{
                reci.setNumRecibo();
                reci.setDomicilio(vista.txtDomicilio.getText());
                reci.setFecha(vista.txtFecha.getText());
                reci.setNombre(vista.txtNombre.getText());
                try{
                    reci.setCostoK(Float.parseFloat(vista.txtCostoK.getText()));
                        reci.setKilowat(Float.parseFloat(vista.txtKilowats.getText()));
                        reci.setTipoServicio(Integer.parseInt(vista.cmbTipoServicio.getSelectedItem().toString()));
                        JOptionPane.showMessageDialog(vista, "Se agrego correctamente");
                        limpiar(); 
                }catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista, "Aparecio el siguiente error: "+ ex.getMessage());
                }
            }
    }
        else if(e.getSource()==vista.btnMostrar){
            vista.txtNumRecibo.setText(String.valueOf(reci.getNumRecibo()));
            vista.txtDomicilio.setText(reci.getDomicilio());
            vista.txtNombre.setText(reci.getNombre());
            vista.txtFecha.setText(reci.getFecha());
            
            vista.txtCostoK.setText(String.valueOf(reci.getCostoK()));
            vista.txtKilowats.setText(String.valueOf(reci.getKilowat()));
            vista.cmbTipoServicio.setSelectedItem(Integer.toString(reci.getTipoServicio()));
            
            vista.txtSubTotal.setText(Float.toString(reci.calcularSubTotal()));
            vista.txtImpuesto.setText(Float.toString(reci.calcularImpuesto()));
            vista.txtTotal.setText(Float.toString(reci.calcularTotal()));
        }
    }

    public static void main(String[] args) {
        Recibo reci = new Recibo();
        dlgVista vista = new dlgVista(new JFrame(), true);
        Controlador control = new Controlador (reci, vista);
        control.iniciarVista();
    }
    
}
