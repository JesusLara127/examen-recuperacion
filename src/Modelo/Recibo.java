package Modelo;

public class Recibo {
    private int numRecibo;
    private String nombre;
    private String domicilio;
    private int tipoServicio;
    private float costoK;
    private float kilowat;
    private String fecha;

    public Recibo() {
        this.numRecibo=0;
        this.nombre="";
        this.domicilio="";
        this.tipoServicio=0;
        this.costoK=0.0f;
        this.kilowat=0.0f;
        this.fecha="";
    }
    public Recibo(int numRecibo, String nombre, String domicilio, int tipoServicio, float costoK, float kilowat, String fecha) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.tipoServicio = tipoServicio;
        this.costoK = costoK;
        this.kilowat = kilowat;
        this.fecha = fecha;
    }
    public Recibo(Recibo aux) {
        this.numRecibo=aux.numRecibo;
        this.nombre=aux.nombre;
        this.domicilio=aux.domicilio;
        this.tipoServicio=aux.tipoServicio;
        this.costoK=aux.costoK;
        this.kilowat=aux.kilowat;
        this.fecha=aux.fecha;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo() {
        this.numRecibo = numRecibo + 1;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
        
    }
    public float getCostoK() {
        return costoK;
    }

    public void setCostoK(float costoK) {
        this.costoK = costoK;
        if(tipoServicio==1){
            costoK=2.00f;
        }
        else if(tipoServicio==2){
            costoK=3.00f;
        }
        else if(tipoServicio==3){
            costoK=5.00f;
        }
    }

    public float getKilowat() {
        return kilowat;
    }

    public void setKilowat(float kilowat) {
        this.kilowat = kilowat;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    public float calcularSubTotal(){
        //float subTotal=0.0f;
        return costoK*kilowat;
    }
    public float calcularImpuesto(){
        return 0.16f*calcularSubTotal();
    }
    public float calcularTotal(){
        return  calcularSubTotal()+calcularImpuesto();
    }
}
